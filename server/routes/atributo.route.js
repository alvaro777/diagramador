const express = require('express')

const _ = require('underscore');
const { json } = require('body-parser');
const Atributo = require('../models/atributo');

const app = express()

app.post('/api/atributo', (req, res) => {

    let body= req.body;


    let atributo= Atributo({
        nombre : body.nombre,
        tipo : body.tipo,
        acceso : body.acceso

    })

    atributo.save((err, atributoDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if(!atributoDB){
            return res.status(400).json({
                ok : false,
                err
            })
        }

        res.json({
            ok : true,
            atributo : atributoDB
        })

    })


})

app.get('/api/atributo/:id', (req, res) => {
    let id= req.params.id;
    Atributo.findById(id)
      .exec((err, atributoDB) => {
         if(err){
            return res.status(500).json({
                ok: false,
                err
            })
         }
         if(!atributoDB){
            return res.status(400).json({
                ok: false,
                err: {
                  message: 'El Id no es valido'
                }
              })
         }res.json({
            ok: true,
            atributo : atributoDB
         })
      })
})




module.exports = app