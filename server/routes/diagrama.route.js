const express = require('express')

// importacion de underscore
const _ = require('underscore');
const { json } = require('body-parser');
const Diagrama = require('../models/diagrama');
const Clase = require('../models/clase');

const app = express()

let listaClases= [];

/*
app.post('/api/diagrama', (req, res) => {

    let body= req.body;

    let input= new DiagramaInput();
    input.nombre= body.nombre;
    input.idsClases= body.idsClases;

    for(let i=0; i< input.idsClases.length; i++){
        Clase.findById(input.idsClases[i])
           .exec((err, claseDB) => {
               if(err) {
                  return res.status(500).json({
                     ok: false,
                     err
                  })
               }
               if(!claseDB){
                  return res.status(400).json({
                     ok: false,
                     err :{
                        message : 'No encontrado'
                     }
                  })
               }
               
               if(claseDB){
                   listaClases.push(claseDB);
               }
           })
    }

    console.log(listaClases);
    


})

*/


app.post('/api/diagrama', (req, res) => {

   let body= req.body; 

   let diagrama = Diagrama({
      nombre : body.nombre,
      clases : body.clases
   })

   diagrama.save((err, diagramaDB) => {
      if(err){
          return res.status(500).json({
              ok: false,
              err
          })
      }

      if(!diagramaDB){
          return res.status(400).json({
              ok : false,
              err
          })
      }

      res.json({
          ok : true,
          diagrama : diagramaDB
      })

   })
})

app.get('/api/diagrama/:id', (req, res) => {
   let id= req.params.id;
   Diagrama.findById(id)
     .exec((err, diagramaDB) => {
        if(err){
           return res.status(500).json({
               ok: false,
               err
           })
        }
        if(!diagramaDB){
           return res.status(400).json({
               ok: false,
               err: {
                 message: 'El Id no es valido'
               }
             })
        }res.json({
           ok: true,
           diagrama : diagramaDB
        })
     })
})





module.exports = app


