const express = require('express')

const _ = require('underscore');
const { json } = require('body-parser');
const Metodo = require('../models/metodo');

const app = express()


app.post('/api/metodo', (req, res) => {
    let body= req.body;

    let metodo= Metodo({
        nombre : body.nombre,
        tipo : body.tipo,
        acceso : body.acceso
    })

    metodo.save((err, metodoDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }
        if(!metodoDB){
            return res.status(400).json({
                ok : false,
                err
            })
        }

        res.json({
            ok: true,
            metodo : metodoDB
        })
    })

})

app.get('/api/metodo/:id', (req, res) => {
    let id= req.params.id;
    Metodo.findById(id)
      .exec((err, metodoDB) => {
         if(err){
            return res.status(500).json({
                ok: false,
                err
            })
         }
         if(!metodoDB){
            return res.status(400).json({
                ok: false,
                err: {
                  message: 'El Id no es valido'
                }
              })
         }res.json({
            ok: true,
            metodo : metodoDB
         })
      })
})




module.exports = app