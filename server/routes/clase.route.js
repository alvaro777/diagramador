const express = require('express')

// importacion de underscore
const _ = require('underscore');
const { json } = require('body-parser');
const Clase = require('../models/clase');

const app = express()

let atributos= [];

/*
app.get('/api/clase', (req, res) => {

    Clase.find({state : true}, '_id nombre, posX posY alto ancho atributos metodos')
      .exec((err, claseDB) => {
        
        if(err){
            return res.status(500).json({
                ok : false,
                err
            })
        }

        if(!claseDB){
            return res.status(400).json({
                ok: false,
                err
            })
        }

        Clase.count({}, (errC, nroClases) => {
            res.json({
                ok: true,
                clases : claseDB,
                nroClases
            })
        })

      })

})
*/

app.post('/api/clase', (req, res) => {

    let body= req.body;

    let clase= Clase({
        nombre : body.nombre,
        posX : body.posX,
        posY : body.posY,
        alto : body.alto,
        ancho : body.ancho,
        atributos: body.atributos,
        metodos : body.metodos
    })

    
    clase.save((err, claseDB) => {
        if(err){
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if(!claseDB){
            return res.status(400).json({
                ok : false,
                err
            })
        }

        res.json({
            ok : true,
            clase : claseDB
        })

    })
    


})

app.get('/api/clase/:id', (req, res) => {
    let id= req.params.id;
    Clase.findById(id)
      .exec((err, claseDB) => {
         if(err){
            return res.status(500).json({
                ok: false,
                err
            })
         }
         if(!claseDB){
            return res.status(400).json({
                ok: false,
                err: {
                  message: 'El Id no es valido'
                }
              })
         }res.json({
            ok: true,
            clase : claseDB
         })
      })
})




module.exports = app
