const express = require('express')

const app = express()

app.use(require('./clase.route'))
app.use(require('./atributo.route'))
app.use(require('./metodo.route'))

app.use(require('./diagrama.route'))

module.exports = app
