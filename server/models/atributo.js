const mongoose = require('mongoose');

var atributoSchema = new mongoose.Schema({
    
    nombre : {
        type : String
    },
    tipo : {
        type : String
    },
    acceso : {
        type : String
    },
    clase : {
        type : mongoose.Schema.Types.ObjectId, ref : 'Clase'
    },
    state: {
        type: Boolean,
        default: true
    },

})


const Atributo = mongoose.model('Atributo', atributoSchema);

module.exports = Atributo;













/*
class Atributo {
    nombre;
    tipo;
    acceso;
}

class Metodo {
    nombre;
    tipo;
    acceso;
}

class Clase {
    nombre;
    posX;
    posY;
    alto;
    ancho;
    atributos;
    metodos;

    diagrama;
}

class Relacion {
    clase1;
    clase2;
}

class Diagrama {
    listaClases;
}
*/