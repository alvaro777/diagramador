const mongoose = require('mongoose');

var metodoSchema = new mongoose.Schema({
    
    nombre : {
        type : String
    },
    tipo : {
        type : String
    },
    acceso : {
        type : String
    },
    clase : {
        type : mongoose.Schema.Types.ObjectId, ref : 'Clase'
    },
    state: {
        type: Boolean,
        default: true
    },

})


const Metodo = mongoose.model('Metodo', metodoSchema);

module.exports = Metodo;