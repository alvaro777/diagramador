const mongoose = require('mongoose');
let Schema = mongoose.Schema

var claseSchema = new mongoose.Schema({
    nombre : {
        type : String
    },
    posX : {
        type : Number
    },
    posY : {
        type : Number
    },
    alto : {
        type : Number
    },
    ancho : {
        type : Number
    },
    atributos : [{
         type : Schema.Types.ObjectId,
         ref: "Atributo"
    }],
    metodos : [{
        type : Schema.Types.ObjectId,
        ref: "Metodo"
   }],
    diagrama : {
        type: Schema.Types.ObjectId, ref: 'Diagrama', 
        default : undefined
    },
    state: {
        type: Boolean,
        default: true
    }
})




const Clase = mongoose.model('Clase', claseSchema);

module.exports = Clase;




/*
class Clase {
    nombre;
    posX;
    posY;
    alto;
    ancho;
    atributos;
    metodos;

    diagrama;
}
*/