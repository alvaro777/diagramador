const mongoose= require('mongoose');
const Schema= mongoose.Schema;

var diagramaSchema= new mongoose.Schema({
    nombre : {
        type : String
    },
    clases : {
        type : [{
            type : Schema.Types.ObjectId,
            ref : "Clase"
        }]
    },
    state: {
        type: Boolean,
        default: true
    },
});


const Diagrama= mongoose.model('Diagrama', diagramaSchema);


module.exports = Diagrama;
// module.exports= diagrama;