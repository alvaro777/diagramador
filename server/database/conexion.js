const mongoose= require('mongoose');

const conexionDB= async() => {
    try{
        //mongodb+srv://alvaro:alvaro@cluster0.dyxtj.mongodb.net/DiagramadorAPIREST
        const con= await mongoose.connect('mongodb://localhost:27017/DiagramadorAPIREST', {
            
        })

        console.log("Conectado a la DB");
        
    }catch(err){
        console.log(err);
    }
}


module.exports= conexionDB;